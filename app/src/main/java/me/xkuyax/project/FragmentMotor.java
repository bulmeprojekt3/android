package me.xkuyax.project;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import lombok.Setter;
import me.xkuyax.project.bluetooth.BluetoothController;
import me.xkuyax.project.util.JoystickView;

public class FragmentMotor extends Fragment {

    @Setter
    private BluetoothController bluetoothController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_motor, container, false);
        JoystickView vertical = (JoystickView) rootView.findViewById(R.id.joystickVertical);
        JoystickView horizontal = (JoystickView) rootView.findViewById(R.id.joystickHorizontal);
        vertical.setVerticalLock(true);
        horizontal.setHorizontalLock(true);
        horizontal.setOnJoystickMoveListener(new JoyStickHandler(horizontal.isHorizontalLock(),bluetoothController), JoystickView.DEFAULT_LOOP_INTERVAL);
        vertical.setOnJoystickMoveListener(new JoyStickHandler(vertical.isHorizontalLock(),bluetoothController), JoystickView.DEFAULT_LOOP_INTERVAL);
        return rootView;
    }
}