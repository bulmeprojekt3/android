package me.xkuyax.project;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import lombok.Setter;
import me.xkuyax.project.bluetooth.BluetoothController;
import me.xkuyax.project.util.DrawImageView;

public class FragmentRGB extends Fragment {

    @Setter
    private BluetoothController bluetoothController;
    private SeekBar seekBarR;
    private SeekBar seekBarG;
    private SeekBar seekBarB;
    private SeekBar seekBarLed;
    private DrawImageView colorPreview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rgb, container, false);
        SeekBar.OnSeekBarChangeListener listener = new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                onRGBUpdate();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        };
        seekBarR = ((SeekBar) rootView.findViewById(R.id.seekBar));
        seekBarG = ((SeekBar) rootView.findViewById(R.id.seekBar2));
        seekBarB = ((SeekBar) rootView.findViewById(R.id.seekBar3));
        seekBarLed = ((SeekBar) rootView.findViewById(R.id.seekBar4));
        colorPreview = (DrawImageView) rootView.findViewById(R.id.colorView);
        seekBarR.setOnSeekBarChangeListener(listener);
        seekBarG.setOnSeekBarChangeListener(listener);
        seekBarB.setOnSeekBarChangeListener(listener);
        seekBarLed.setOnSeekBarChangeListener(listener);
        return rootView;
    }

    public void onRGBUpdate() {
        int led = seekBarLed.getProgress();
        int r = seekBarR.getProgress();
        int g = seekBarG.getProgress();
        int b = seekBarB.getProgress();
        colorPreview.setR(r);
        colorPreview.setG(g);
        colorPreview.setB(b);
        colorPreview.invalidate();
        if (bluetoothController != null) {
            bluetoothController.writeBytes(new byte[]{(byte) 0, (byte) led, (byte) (255 - r), (byte) (255-g), (byte) (255-b)});
        }
    }
}