package me.xkuyax.project.util;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ImageView;

import lombok.Getter;
import lombok.Setter;


public class DrawImageView extends ImageView {

    private Paint paint;
    private Rect rectangle;
    @Getter
    @Setter
    private int r, g, b;

    public DrawImageView(Context context) {
        super(context);
    }

    public DrawImageView(Context context, AttributeSet set) {
        super(context, set);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.paint = new Paint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (rectangle == null) {
            this.rectangle = new Rect(0, 0, getRight(), getBottom());
        }
        paint.setColor(Color.rgb(r, g, b));
        canvas.drawRect(rectangle, paint);
    }
}
