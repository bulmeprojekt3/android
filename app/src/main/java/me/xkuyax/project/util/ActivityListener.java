package me.xkuyax.project.util;

import android.app.Activity;

public interface ActivityListener {

    void onCreate(Activity activity);

    void onResume(Activity activity);

    void onPause(Activity activity);

}
