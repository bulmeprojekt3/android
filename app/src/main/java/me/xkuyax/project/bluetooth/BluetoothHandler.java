package me.xkuyax.project.bluetooth;

import android.os.Handler;

public class BluetoothHandler extends Handler {

    private StringBuilder message = new StringBuilder();

    public void handleMessage(android.os.Message bluetoothMessage) {
        switch (bluetoothMessage.what) {
            case BluetoothController.RECIEVE_MESSAGE:
                byte buffer[] = (byte[]) bluetoothMessage.obj;
                for (int i = 0; i < bluetoothMessage.arg1; i++) {
                    byte b = buffer[i];
                    message.append((char) b);
                    if (b == '\n') {
                        String s = message.toString().replaceAll("\n|\r", "");
                        System.out.println("Arduino sagt: " + s);
                        message.delete(0, message.length());
                    }
                }
                break;
        }
    }

    ;

}
