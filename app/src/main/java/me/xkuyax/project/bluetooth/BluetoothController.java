package me.xkuyax.project.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.UUID;

import me.xkuyax.project.MainActivity;

public class BluetoothController {

    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    public static final int RECIEVE_MESSAGE = 1;
    private String address;
    private Handler handler;
    private ConnectedThread connectedThread;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothSocket btSocket = null;

    public BluetoothController(Handler handler, String address) {
        this.handler = handler;
        this.address = address;
    }

    public void writeBytes(byte[] msgBuffer) {
        System.out.println(Arrays.toString(msgBuffer));
        connectedThread.writeBytes(msgBuffer);
    }


    public void write(String message) {
        connectedThread.write(message);
    }

    public void onCreate(Activity activity) {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        checkBTState(activity);
    }

    public void onResume(Activity activity) {
        Log.d(MainActivity.APP_TAG, "...onResume - try connect...");
        BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);
        try {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            errorExit("Fatal Error", "In onResume() and socket create failed: " + e.getMessage() + ".", activity);
        }
        bluetoothAdapter.cancelDiscovery();
        Log.d(MainActivity.APP_TAG, "...Connecting...");
        try {
            btSocket.connect();
            Log.d(MainActivity.APP_TAG, "....Connection ok...");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Retrying connection!");
            try {
                Thread.sleep(100l);
                btSocket.connect();
                Log.d(MainActivity.APP_TAG, "....Connection 2 ok...");
            } catch (IOException e1) {
                e1.printStackTrace();
                System.out.println("Printing io ");
                try {
                    btSocket.close();
                } catch (IOException e2) {
                    errorExit("Fatal Error", "In onResume() and unable to close socket during connection failure" + e2.getMessage() + ".", activity);
                }
               // errorExit("Fatal Error", "Could not setup bluetooth connection", activity);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
        Log.d(MainActivity.APP_TAG, "...Create Socket...");
        connectedThread = new ConnectedThread(btSocket);
        connectedThread.start();
    }

    public void onPause(Activity activity) {
        Log.d(MainActivity.APP_TAG, "...In onPause()...");
        try {
            btSocket.close();
        } catch (IOException e2) {
            errorExit("Fatal Error", "In onPause() and failed to close socket." + e2.getMessage() + ".", activity);
        }
    }

    private void checkBTState(Activity activity) {
        if (bluetoothAdapter == null) {
            errorExit("Fatal Error", "Bluetooth not support", activity);
        } else {
            if (bluetoothAdapter.isEnabled()) {
                Log.d(MainActivity.APP_TAG, "...Bluetooth ON...");
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                activity.startActivityForResult(enableBtIntent, 1);
            }
        }
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        if (Build.VERSION.SDK_INT >= 10) {
            try {
                final Method m = device.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", new Class[]{UUID.class});
                return (BluetoothSocket) m.invoke(device, MY_UUID);
            } catch (Exception e) {
                Log.e(MainActivity.APP_TAG, "Could not create Insecure RFComm Connection", e);
            }
        }
        return device.createRfcommSocketToServiceRecord(MY_UUID);
    }

    private void errorExit(String title, String message, Activity activity) {
        Toast.makeText(activity.getBaseContext(), title + " - " + message, Toast.LENGTH_LONG).show();
        activity.finish();
    }

    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
            }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[256];
            int bytes;
            while (true) {
                try {
                    bytes = mmInStream.read(buffer);
                    handler.obtainMessage(RECIEVE_MESSAGE, bytes, -1, buffer).sendToTarget();
                } catch (IOException e) {
                    break;
                }
            }
        }

        public void writeBytes(byte[] msgBuffer) {
            Log.d(MainActivity.APP_TAG, "...Data to send: " + Arrays.toString(msgBuffer) + "...");
            try {
                mmOutStream.write(msgBuffer);
            } catch (IOException e) {
                Log.d(MainActivity.APP_TAG, "...Error data send: " + e.getMessage() + "...");
            }
        }

        public void write(String message) {
            Log.d(MainActivity.APP_TAG, "...Data to send: " + message + "...");
            byte[] msgBuffer = message.getBytes();
            try {
                mmOutStream.write(msgBuffer);
            } catch (IOException e) {
                Log.d(MainActivity.APP_TAG, "...Error data send: " + e.getMessage() + "...");
            }
        }
    }
}
