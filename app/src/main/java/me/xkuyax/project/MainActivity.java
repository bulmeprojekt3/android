package me.xkuyax.project;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import me.xkuyax.project.bluetooth.BluetoothController;
import me.xkuyax.project.bluetooth.BluetoothHandler;

public class MainActivity extends AppCompatActivity {

    public static String APP_TAG = "Projekt";
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private BluetoothController bluetoothController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        bluetoothController = new BluetoothController(new BluetoothHandler(), "98:D3:31:20:47:52");
        bluetoothController.onCreate(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bluetoothController.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        bluetoothController.onPause(this);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                FragmentMotor fragmentMotor = new FragmentMotor();
                fragmentMotor.setBluetoothController(bluetoothController);
                return fragmentMotor;
            }
            if (position == 1) {
                FragmentRGB fragmentRGB = new FragmentRGB();
                fragmentRGB.setBluetoothController(bluetoothController);
                return fragmentRGB;
            }
            throw new RuntimeException("Somewhat tried to getItem with invalid position!");
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "MOTOR";
                case 1:
                    return "RGB";
            }
            return null;
        }
    }
}
