package me.xkuyax.project;

import lombok.Getter;
import me.xkuyax.project.bluetooth.BluetoothController;
import me.xkuyax.project.util.JoystickView;
import me.xkuyax.project.util.Utils;

@Getter
public class JoyStickHandler implements JoystickView.OnJoystickMoveListener {

    private final boolean horizontal;
    private final BluetoothController bluetoothController;
    private int lastSpeed;
    private int lastSpeedNegative;
    private int lastServo;

    public JoyStickHandler(boolean horizontal, BluetoothController handler) {
        this.horizontal = horizontal;
        this.bluetoothController = handler;
    }

    @Override
    public void onValueChanged(int angle, int power, int direction) {
        boolean negative = isHorizontal() ? angle < 0 : angle > 0;
        int nn = negative ? -1 : 1;
        int rpower = nn * power;
        if (isHorizontal()) {
            lastServo = (int) Utils.map(rpower, -100, 100, 1460, 900);
            bluetoothController.writeBytes(new byte[]{(byte) 2, (byte) (lastServo >> 8), (byte) (lastServo & 0xFF)});
        } else {
            if (rpower > 50 || rpower <=-50) {
                lastSpeed = Math.abs(rpower);
                lastSpeedNegative = negative ? 1 : 0;
            } else {
                lastSpeed = 0;
                lastSpeedNegative = 0;
            }
            bluetoothController.writeBytes(new byte[]{(byte) 1, (byte) lastSpeedNegative, (byte) lastSpeed});
        }
    }
}
